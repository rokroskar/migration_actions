#!/home/ics/roskar/anaconda/bin/python
import pynbody
import sys
sys.path.append('/home/ics/roskar/nbody/projects/migration_actions')
import migration_actions
pynbody.config['number_of_threads'] = 16

s = pynbody.load(sys.argv[1])
pynbody.analysis.angmom.faceon(s,mode='ssc')
jr,lz,jz = migration_actions.get_single_output_actions(s)
s.s['jr_act'] = jr
s.s['lz_act'] = lz
s.s['jz_act'] = jz
for arr in ['jr_act','lz_act','jz_act'] : 
    s.s[arr].write(overwrite=True)
