"""
Routines for studying evolution of actions in radial 
migration scenarios.

The repository is https://bitbucket.org/rokroskar/migration_actions

see also the IPython notebook actions_of_particles_in_simulations.ipynb
"""


import galpy
import numpy as np
import pynbody

from galpy.potential import SnapshotPotential, InterpSnapshotPotential
from galpy.potential import interpRZPotential
from galpy.actionAngle import actionAngleStaeckel, actionAngleAdiabatic

import os

def get_single_output_actions(s) : 
    """
    Use galpy to calculate the actions in the snapshot `s`. 
    This assumes `s` is already centered and aligned properly
    s.t. the disk is in the x-y plane. 

    Input:
    ------

    s : pynbody simulation snapshot

    """

    # generate the interpolated potential 
    print 'Generating potential grid...'
    interp_pot = InterpSnapshotPotential(s,numcores=16,
                                         rgrid = (np.log(0.01),
                                                  np.log(20.),51), 
                                         zgrid=(0.,10.0,51),
                                         logR=True,
                                         interpepifreq=True,
                                         interpverticalfreq=True,
                                         enable_c=True)
    
    # get the actions
    print 'Computing the actions...'
    aAS= actionAngleStaeckel(pot=interp_pot,delta=0.45,c=True)
    rxy,vr,vt,z,vz = [np.ascontiguousarray(s.s[x]) for x in ('rxy','vr','vt','z','vz')]
    jr,lz,jz= aAS(rxy,vr,vt,z,vz)

    return jr, lz, jz


def submit_single_output_action_to_slurm(filename) : 
    """
    Generates a simple SLURM script and submits the python 
    job to calculate actions for a single output. 
    """

    f = open(filename+'_actions.slurm','w')
    f.write('#!/bin/bash\n')
    f.write('#SBATCH --ntasks=16\n')
    f.write('#SBATCH --time=01:00:00\n')
    f.write('python ~/nbody/projects/migration_actions/single_output_actions.py %s\n'%filename)
    f.close()
    
    os.system('sbatch %s_actions.slurm'%filename)
